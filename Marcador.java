/*Objeto java simple
    Plain Old Java Object (POJO)
    JavaBeans
*/
public class Marcador{
    /*Atributos de la clase*/
    private String color;    
    private int tamano;
    private String tipo;
    private String marca;

    /* Metodo constructor por defecto, permite inicializar
       los atributos de los objetos instaciados
    */
    public Marcador(){        
    }

    public Marcador(String color, int tamano, String tipo,String marca){
        this.color = color;
        this.tamano = tamano;
        this.tipo = tipo;
        this.marca = marca;
    }
    public void setColor(String color){
       this.color = color;
    }

    public String getColor(){
        return this.color;
    }

    public void setTamano(int tamano){
        this.tamano = tamano;
    }

    public int getTamano(){
        return this.tamano;
    }

    public void setTipo(String tipo){
        this.tipo = tipo;
    }

    public String getTipo(){
        return this.tipo;
    }

    public void setMarca(String marca){
        this.marca = marca;
    }

    public String getMarca(){
        return this.marca;
    }

}